"""
Author: Vaidyanath Areyur Shanthakumar
Created: 06/2020
Description: NOTE:********This code takes all baseline files in a directory and creates target files to run nearest neighbors. Functions in validation_utils.py superseeds this. NOTE***** Use only if needed to validate for a large number of baseline files.
"""

import tensorflow as tf
import pickle
import h5py
from collections import defaultdict
from time import time
import numpy as np
import pandas as pd
from os import listdir
from os.path import isfile, join
import os
import ntpath

predicted_mf_embeddings_file = "concat_se_better_final_mf_embeddings.h5py"
validation_path = "/usr/app/Transformer/data/"
output_path = "/usr/app/Transformer/concat_se_mf_output/"


def timer(func):
    '''
    A decorator function to time the function calls
    '''
    def new_func(*args, **kw):
        start_time = time()
        result = func(*args, **kw)
        end_time = time()
        print("Total time to run "
              + func.__name__ + " = "
              + str(end_time - start_time)
              + "s")
        return result
    return new_func

#utility function to extract file_name from a path
def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

#function that takes a list with repeated elements and outputs a list with distinct elements preserving the order
def get_unique(arr):
    unique = list()
    for i in arr:
        if i not in unique:
            unique.append(i)
    return unique

@timer
def main():
    all_dirs = [x[0] for x in os.walk(validation_path)]

    all_dirs.pop(0)
    all_dirs.pop(len(all_dirs)-1)
    all_dirs.pop(0) #####excluding recs_random folder
    print(all_dirs)
    with h5py.File(predicted_mf_embeddings_file, 'r') as m:
        print("Reading embeddings in progress...")
        product_ids = m['p_id'][:]
        emb = m['emb'][:]
        se_tup = list(zip(product_ids,emb))
        se_df = pd.DataFrame(se_tup, columns=['p_id', 'embed'])
        print("Reading embeddings successful")
        for d in all_dirs:
            just_files = [f for f in listdir(d) if isfile(join(d, f))]
            onlyfiles = [join(d,f) for f in listdir(d) if isfile(join(d, f))]
            one_file = list()
            one_file.append(onlyfiles[len(onlyfiles)-1])
            for k,f in enumerate(one_file):
                print("Working on file: " + f)
                if(os.stat(f).st_size == 0):
                    print(f + "is empty")
                    continue
                df = pd.read_csv(f, sep='\t', names = ['target', 'recs', 'score'])
                target_ids = (df['target'].tolist())
                target_ids = get_unique(target_ids)
                print("The number of unique target_ids = " + str(len(target_ids)))
                target_df = pd.DataFrame(target_ids, columns =['p_id'])
                test_df = target_df.merge(se_df, on='p_id')
                prod_ids = np.asarray(test_df['p_id'])
                embeds = np.asarray(test_df['embed'].values.tolist())
                target_file_path = output_path + path_leaf(d) + "/" +path_leaf(f) +  "_mf_predict_test.h5py"
                ff = h5py.File(target_file_path, 'w')
                pid_set = ff.create_dataset("p_id", (test_df.shape[0],), dtype='i8')
                emb_set = ff.create_dataset("emb", (test_df.shape[0],emb.shape[1]), dtype='f8')
                pid_set[:] = [prod_ids]
                emb_set[: , :] =(embeds)
                ff.close()
                print("Writing to file successful: " + target_file_path)
    m.close()

if __name__ == "__main__":
    main()