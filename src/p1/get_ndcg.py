from metrics import *

val_bm_file = "/usr/app/Transformer/data/recs/recs941_lowest_20k_targets_excl_expanded_930_incl_839_2020_04_18_T3"
corres_test_file = "/usr/app/Transformer/data/recs/recs941_lowest_20k_targets_excl_expanded_930_incl_839_2020_04_18_T3_mf_predict_output_pairs.tsv"

recs = [
    (1, 10, 1.0),
    (1, 20, 0.8),
    (1, 30, 0.9),
    (1, 40, 0.5),
    (2, 10, 0.1),
    (2, 20, 0.2),
    (2, 30, 0.3),
    (3, 10, 1.0),
    (4, 10, 0.5)
]
recs = pd.DataFrame(recs, columns = ['target', 'rec', 'score'])

labels = [
    (1, 10, 1.0),
    (1, 20, 1.0),
    (2, 10, 1.0),
    (2, 20, 1.0),
    (2, 50, 1.0),
    (3, 10, 1.0),
    (5, 10, 1.0)
]
labels = pd.DataFrame(labels, columns = ['target', 'rec', 'rel'])

print(compute_metrics(recs, labels, NDCG(2)))