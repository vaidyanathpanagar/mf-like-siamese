"""
Author: Vaidyanath Areyur Shanthakumar
Created: 06/2020
Description: This script takes two h5py files with embeddings and generates another file with concatenated embeddings from file1 and file 2
"""
import math
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model
import statistics
import pandas as pd
import pickle
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from time import time
import h5py

se_embed_file = "/usr/app/Transformer/temp_storage/reduced_original_se_embeddings_300D_20200418_4mil_count.h5py"
pred_mf_embeds_file = "/usr/app/Transformer/temp_storage/pred_mf_embeddings_from_model_simple_se_to_mf_cosine_loss_0_82.h5py"
output_file_path = "/usr/app/Transformer/temp_storage/concat_original_se_plus_pred_mf_from_model_simple_se_to_mf_cosine_loss_0_82.h5py"

def timer(func):
    '''
    A decorator function to time the function calls
    '''
    def new_func(*args, **kw):
        start_time = time()
        result = func(*args, **kw)
        end_time = time()
        print("Total time to run "
              + func.__name__ + " = "
              + str(end_time - start_time)
              + "s")
        return result
    return new_func


f = h5py.File(se_embed_file, 'r')
f2 = h5py.File(pred_mf_embeds_file, 'r')
steps = 10000
last_batch_size = f['p_id'].shape[0]%steps
i=0
ff = h5py.File(output_file_path, 'w')
pid_set = ff.create_dataset("p_id", (f['p_id'].shape[0],), maxshape=(int(15e6),), dtype='i8')

# replace the 400 here with the total length of concatenated embeddings
emb_set = ff.create_dataset("emb", (f['p_id'].shape[0],400), maxshape=(int(15e6),400), dtype='f8')

while(i< f['p_id'].shape[0]):
    prod_ids = f['p_id'][i:i+steps]
    emb = f['emb'][i:i+steps]
    emb2 = f2['emb'][i:i+steps]
    input_data = np.asarray(emb)
    input_data2 = np.asarray(emb2)
    new_emb = np.concatenate((input_data,input_data2),axis=1)
    pid_set[i:i+steps] = [prod_ids]
    emb_set[i:i+steps,:] =(new_emb)
    print(i)
    i=i+steps
print("Last batch computing....")
prod_ids = f['p_id'][last_batch_size:]
emb = f['emb'][last_batch_size:]
emb2 = f2['emb'][last_batch_size:]
input_data = np.asarray(emb)
input_data2 = np.asarray(emb2)
#pred_data = model.predict(input_data)
new_emb = np.concatenate((input_data,input_data2),axis=1)
pid_set[last_batch_size:] = [prod_ids]
emb_set[last_batch_size:,:] =(new_emb)
ff.close()
f.close()
f2.close()
print("Hurray! Everything Successful!")