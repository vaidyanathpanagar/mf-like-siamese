import numpy as np
import pandas as pd
import pickle
import json
import tensorflow as tf
from sklearn.preprocessing import StandardScaler
import h5py
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

file_path = "/usr/app/Transformer/data/filtered_prod_features.json"
output_file_path = "/usr/app/Transformer/product_attr_97_filtered_260k_count.p"
p_att_data = pd.read_json(file_path, lines=True)
p_att_data.dropna(inplace=True)


col_list = list()
for col,dt in zip(p_att_data.columns, p_att_data.dtypes):
    if(dt == np.float64 or dt == np.int64):
        col_list.append(col)

reduced_p_feat_data = p_att_data[col_list]
pickle.dump(reduced_p_feat_data, open(output_file_path, "wb"))
print("Successfully Saved!")