"""
Author: Vaidyanath Areyur Shanthakumar
Date created: 07/2020
Description: This file contains some util functions that help in validation operations


"""


import tensorflow as tf
import pickle
import h5py
from collections import defaultdict
from time import time
import numpy as np
import pandas as pd
from os import listdir
from os.path import isfile, join
import os
import ntpath
import math
from time import time
import keras
from metrics import *

def timer(func):
    '''
    A decorator function to time the function calls
    '''
    def new_func(*args, **kw):
        start_time = time()
        result = func(*args, **kw)
        end_time = time()
        print("Total time to run "
              + func.__name__ + " = "
              + str(end_time - start_time)
              + "s")
        return result
    return new_func

@timer
def get_unique(arr):
    """
        Description: This function takes a list as input and returns the distinct elements of the list
        Note: This function preserves the order of elements in the list. If order is unimportant, I suggest you use set() function to get distinct elements of a list. It is much faster.
    """
    unique = list()
    for i in arr:
        if i not in unique:
            unique.append(i)
    return unique


@timer
def get_common(list1, list2):
    """
    Description: returns an intersection of two lists, PRESERVING ORDER
    """
    flag = True
    com_list = list()
    unique_list1 = get_unique(list1)
    unique_list2 = get_unique(list2)
    unl_len1 = len(unique_list1)
    unl_len2 = len(unique_list2)
    if(unl_len2 > unl_len1):
        flag = False
    if(flag):
        for pid in unique_list1:
            if pid in unique_list2:
                com_list.append(pid)
    else:
        for pid in unique_list2:
            if pid in unique_list1:
                com_list.append(pid)
    
    return com_list

@timer
def close_all_h5py_files():
    """
    Sometimes there will be trouble opening/reading/writing h5py files if the file was already open somewhere 
    and was not closed properly, etc. This code closes all open h5py files.
    If and when an error is encountered while opening,reading,writing hypy files, run this code once and then try again
    
    NOTE: THIS CODE CLOSES ALL .h5py FILES. USE IT AS THE LAST RESORT.
    """
    import gc
    for obj in gc.get_objects():   # Browse through ALL objects
        if isinstance(obj, h5py.File):   # Just HDF5 files
            try:
                obj.close()
            except:
                pass # Was already closed

@timer
def get_dataset_names(file, pid_name = None, embeddings_name = None):
    """
    Helper function to obtain the names of the dataset in a h5py file that correspond to product_ids
    and embeddings. 
    Note: Only tested and works perfectly for h5py files with two datasets. 
    If there are more datasets, then proper dataset names should be supplied explicitly
    """
    z =  h5py.File(file, 'r')
    f = z
    names = list(f.keys())
    if(len(names)>2):
        print("WARNING!!!\nWARNING!!!\nWARNING!!!\nWARNING!!!\nWARNING!!!: This file has more than 2 datasets. Please explicitly specify the names of datasets containing product_ids and emnbeddings respectively as the last two arguments of this function call. If you have not, then, this may fail or give erroneous results")
    if(len(list(f[names[0]].shape)) == 2):
        embedding_name = names[0]
        pid_name = names[1]
    else:
        embedding_name = names[1]
        pid_name = names[0]
#     print("embeddings dataset name is: " + embedding_name)
#     print("product_id dataset name is: " + pid_name)
    z.close()
    return pid_name, embedding_name
    


@timer
def poor_filter_embeddings(to_be_filtered_file, all_embeddings_file, output_path, CHUNKSIZE=1e6):
    """
    Description:This function takes two files, one with to be filtered target_ids and all_embeddings_file and filters only those embeddings corresponding to the target_ids in tbf_file
    to_be_filtered_file: a .txt file with only product_ids that need to be filtered
    all_embeedings_file: a h5py file containing all the embeddings from which filtering needs to be done
    output_path: a path name where the new h5py file with filtered embeddings need to be saved 
    CHUNKSIZE: Size of chunks to read all_embeddings_file (Depends on memory availability, bigger the better, defaults at 1e6)
    
    ***Note: to_be_filtered_file can be a .txt file or a dataframe or .h5py file (with a dataset for product_ids and a dataset for embeddings)*** 
    
    Time: sample_use_case: To filter out ~3.5mil embeddings from ~14mil embeddings, This function took about 
    ***20 minutes***
    
    NOTE:
    NOTE:
    NOTE: This is usually suitable for large files (10mil + samples) with high dimensional embeddings (250+). This is
    SLOW. If good amount of memory is available or the file size is smaller, it is quicker to use rich_filter_embeddings()
    function to achieve this goal. rich_filter_embeddings() usually does it in <5 minutes for the same sample use case.
    """
    embed_dim = 0
    CHUNKSIZE = int(CHUNKSIZE)
    print("Reading to be filtered prod_ids file")
    if(isinstance(pp_input_data, pd.DataFrame)):
        print("Dataframe recognized")
        tbf_df = to_be_filtered_file
    elif(to_be_filtered_file.endswith('.txt')):
        print(".txt file recognized")
        tbf_df = pd.read_csv(to_be_filtered_file, names= ['p_id'])
#     print("Reading to be filtered prod_ids file Successful")
#     print("The reduced se df shape is: " + str(tbf_df.shape))
        r_se_pid = tbf_df['p_id'].values.tolist()
        p_id = [int(p) for p in r_se_pid]
        tbf_df = pd.DataFrame(r_se_pid, columns=["p_id"])
    elif(to_be_filtered_file.endswith('.h5py')):
        print(".h5py file recognized")
        p_name, e_name = get_dataset_names(to_be_filtered_file)
        fh = h5py.File(all_embeddings_file, 'r')
        p_id = fh[p_name][:]
        p_id = [int(p) for p in p_id]
        tbf_df = pd.DataFrame(p_id, columns=["p_id"])
    else:
        print("Couldn't determine the type of file! Please create a pandas dataframe with all the product_ids and pass it as p_id_df argument")
        print("Also, name the column 'p_id'")
        
    
    print("The no. of product_ids to be filtered is:" + str(tbf_df.shape[0]))
    
    print("Reading from all_embeddings_file")
    with h5py.File(all_embeddings_file, 'r') as f:
        pid_name, embed_name = get_dataset_names(all_embeddings_file)
        embed_dim = f[embed_name].shape[1]
        prod_ids = (f[pid_name][:])
        print("Reading from all_embeddings_file Successful!!!")

    prod_ids = [int(p) for p in prod_ids]

    p_index = list()
    for i in range(len(prod_ids)):
        p_index.append(i)

    p_index = pd.DataFrame(p_index, columns=["p_index"])
    p_index['p_id'] = prod_ids
    
    final_df = tbf_df.merge(p_index, on = 'p_id')

    print("The final dataframe shape is: " + str(final_df.shape))
    
    p_index_list = final_df['p_index'].values.tolist()
    p_index_list.sort()
    
    
    print("Preparing to filter...")
    no_of_chunks = math.ceil(len(prod_ids)/CHUNKSIZE)
    lists = [[] for i in range(no_of_chunks)]
    
    for i in p_index_list:
        l_no = math.floor(i/1e6)
        lists[l_no].append(i)
    
    print("Preparing to filter...Completed!!!")
    low_bound = int(0)
    upper_bound = int(0 + CHUNKSIZE)
    k = 0
    cur_k = 0
    l_no = 0
    print("Creating Output File...")
    ff = h5py.File(output_path, 'w')
    pid_set = ff.create_dataset("p_id", (final_df.shape[0],), dtype='i8')
    emb_set = ff.create_dataset("emb", (final_df.shape[0],embed_dim), dtype='f8')
    print("Successfully opened Output file")
    
    z = h5py.File(all_embeddings_file, 'r')
    pid_name, embed_name = get_dataset_names(all_embeddings_file)
    print(z[pid_name].shape[0])
    # total_chunks = math.ceil(z['p_id'].shape[0]/CHUNKSIZE)
    total_chunks = len(lists)
    print("The total number of chunks is: " + str(total_chunks))
    
    for l in range(total_chunks):
        print("The low_bound is:" + str(low_bound))
        print("The upper_bound is:" + str(upper_bound))

        prod_id = z[pid_name][low_bound:upper_bound+1]
        embed = z[embed_name][low_bound:upper_bound+1]
        for i in lists[l]:
            pid_set[k] = prod_id[i - low_bound]
            emb_set[k] = embed[i - low_bound]
            k+=1
            if(k%100000 == 0):
                print(k)
        low_bound = int(low_bound + CHUNKSIZE)
        upper_bound = int(upper_bound + CHUNKSIZE)

    print("Successfully created the file")
    ff.close()
    z.close()

@timer
def target_h5py_file(baseline_recs_file, target_file_path, se_df, embedding_dim):
    """
    This is a function that creates a h5py file with target product_ids from the baseline_recs_file and corresponding
    embeddings from the se_df (obtained from create_target_h5py_file() function)
    embedding_dim is the actual dimension of the embeddings w.r.t. se_df (supplied by the create_target_h5py_file() 
    function)
    NOTE: Read the description of create_target_h5py_file() function to get better insights into this function
    """
    f = baseline_recs_file
    
    if(os.stat(baseline_recs_file).st_size == 0):
        print(baseline_recs_file + "is empty")
        print("Cannot Continue :(:(:(:(:")
        return
    else:
        df = pd.read_csv(f, sep='\t', names = ['target', 'recs', 'score'])
        target_ids = (df['target'].tolist())
        target_ids = get_unique(target_ids)
        print("The number of unique target_ids = " + str(len(target_ids)))
        target_df = pd.DataFrame(target_ids, columns =['p_id'])
        test_df = target_df.merge(se_df, on='p_id')
        prod_ids = np.asarray(test_df['p_id'])
        embeds = np.asarray(test_df['embed'].values.tolist())
        #target_file_path = output_path + path_leaf(d) + "/" + path_leaf(f) +  "_mf_predict_test.h5py"
        print("Creating target file... ")
        ff = h5py.File(target_file_path, 'w')
        pid_set = ff.create_dataset("p_id", (test_df.shape[0],), dtype='i8')
        emb_set = ff.create_dataset("emb", (test_df.shape[0],embedding_dim), dtype='f8')
        print("Creating target file successful!!!")
        print("Writing to file")
        pid_set[:] = [prod_ids]
        emb_set[: , :] =(embeds)
        ff.close()
        print("Writing to file successful: " + target_file_path)

@timer
def create_target_h5py_file(baseline_recs_file, target_file_path, all_embeddings_file, pid_name=None, embedding_name=None):
    """
    Creates a h5py file (to be used in nearest neighbors calculation)with two datasets: 
    p_id- product ids of all products in targets of baseline file
    emb- coressponding embeddings
    parameters: 
    baseline_recs_file: the baseline files from which targets are received
    target_file_path: file name in which the embeddings for the targets received will be stored (this will be used for nearest neighbors calculation) 
    all_embeddings_file: the h5py file that contains embeddings for all possible target_ids and their embeddings
    Note: If the all_embeddings_file contains more than two datasets(product_id and embeddings), it needs to be 
    explicitly specified what datasets contain product_ids and embeddings with the below arguments:
    pid_name
    embedding_name:
    """
    z =  h5py.File(all_embeddings_file, 'r')
    f = z
    names = list(f.keys())
    if(len(names)>2 and pid_name==None and embedding_name==None):
        print("WARNING!!!\nWARNING!!!\nWARNING!!!\nWARNING!!!\nWARNING!!!: This file has more than 2 datasets. Please explicitly specify the names of datasets containing product_ids and emnbeddings respectively as the last two arguments of this function call. If you have not, then, this may fail or give erroneous results")
    if(len(list(f[names[0]].shape)) == 2):
        embedding_name = names[0]
        pid_name = names[1]
    else:
        embedding_name = names[1]
        pid_name = names[0]
    print("embeddings dataset name is: " + embedding_name)
    print("product_id dataset name is: " + pid_name)
        
    product_ids = f[pid_name][:]
    emb = f[embedding_name][:]
    
    embedding_dim = f[embedding_name].shape[1]
    
    se_tup = list(zip(product_ids,emb))
    se_df = pd.DataFrame(se_tup, columns=['p_id', 'embed'])
        
    target_h5py_file(baseline_recs_file, target_file_path, se_df, embedding_dim)
    z.close()


def create_nearest_neigh_command(all_embeddings_file, target_embeddings_file, output_path):
    """
    a decorator function to help create nearest neighbors command
    parameters:
    all_embeddings_file: the file that contains product_ids and embeddings of all possible candidates
    target_embeddings_file: file that contains product_ids and embeddings for targets
    output_path
    
    Note: Provide an example command by changing the cmd_string variable to modify this to your needs
    """
    
    cmd_string = "docker run --rm -v /mldev/:/usr/app docker.overstock.com/ml/nearest-neighbors:0.2.2 search --index \"L2norm,HNSW32\" --embedding /usr/data/final_mf_predictions.h5py     --ids /usr/data/final_mf_predictions.h5py     --examples-file /usr/data/output_data/recs/recs941_lowest_20k_targets_excl_expanded_930_incl_839_2020_04_18_T3_mf_predict_test.h5py     --examples-ids /usr/data/output_data/recs/recs941_lowest_20k_targets_excl_expanded_930_incl_839_2020_04_18_T3_mf_predict_test.h5py     -a ids_key='p_id'     -a embedding_key='emb'     -a examples_ids_key='p_id'     -a examples_key='emb'     -a k=100     -o /usr/data/predicted_sets/recs/recs941_lowest_20k_targets_excl_expanded_930_incl_839_2020_04_18_T3_mf_predict_output -c 100000 -p" 
    cmd_split = cmd_string.split()
#     for i,w in enumerate(cmd_split):
#         print(i,w)
    cmd_list = cmd_split.copy()
    cmd_list[10] = all_embeddings_file
    cmd_list[12] = all_embeddings_file
    pid, emb = get_dataset_names(all_embeddings_file)
    
    d_name_cmd = cmd_list[18].split('=')
    d_name_cmd[1] = "=" + "'"+ pid +"'"
    cmd_list[18] = "".join(d_name_cmd)
    
    d_name_cmd = cmd_list[20].split('=')
    d_name_cmd[1] = "=" + "'"+ emb +"'"
    cmd_list[20] = "".join(d_name_cmd)
    
    pid, emb = get_dataset_names(target_embeddings_file)
    d_name_cmd = cmd_list[22].split('=')
    d_name_cmd[1] = "=" + "'"+ pid +"'"
    cmd_list[22] = "".join(d_name_cmd)
    
    d_name_cmd = cmd_list[24].split('=')
    d_name_cmd[1] = "=" + "'"+ emb +"'"
    cmd_list[24] = "".join(d_name_cmd)
    
    cmd_list[14] = target_embeddings_file
    cmd_list[16] = target_embeddings_file
    cmd_list[28] = output_path
    final_cmd = " ".join(cmd_list)
    print(final_cmd)

@timer
def trim_baseline_file(baseline_file, recs_file):
    """
    Description: This function evens out the number of targets (and their recs, scores) in baseline files to their
    corresponding nearest neighbors service generated recs file
    parameters:
    baseline_file: baseline file
    recs_file: nearest neighbors service generated recs file
    """
    df = pd.read_csv(baseline_file, sep='\t', names = ['target', 'rec', 'score'])
    df2 = pd.read_csv(recs_file, sep='\t', names = ['target', 'rec', 'score'])
    
    print("The original baseline file shape is: " + str(df.shape))
    print("The generated recs file shape is: " + str(df2.shape))
    
    t1 = set(df['target'].values.tolist())
    t2 = set(df2['target'].values.tolist())
    
    missing_targets = list(t1 - t2)
    
    for i in missing_targets:
        df = df[df.target != i]
    
    
    print("The trimmed baseline file shape is: " + str(df.shape))
    
    return df

@timer
def tabulate_ndcg(truth_file, generated_recs, k, b_df = False):
    """
    Tabulates NDCG@k score for a generated_recs.tsv file w.r.t. truth_file
    truth_file: Ideal recs file
    generated_recs: file or trimmed baseline_df
    k: value at which NDCG needs to be calculated (NDCG@k)
    baseline_df: True if a dataframe is being passed at "generated_recs" argument, default is False (a file is passed)
    """
    if(not b_df):
        val_df = pd.read_csv(generated_recs, sep='\t', names = ['target', 'rec', 'score'])
    else:
        val_df = generated_recs
    
    cp_df = pd.read_csv(truth_file, sep='\t', names = ['target', 'rec', 'rel'])
    score = compute_metrics(val_df, cp_df, NDCG(k))
    print("NDCG@" + str(k) + " = " + str(score))
    return score

@timer
def rich_filter_embeddings(to_be_filtered_file, all_embeddings_file, target_file_path):
    """
    This function takes two files, one with to be filtered target_ids and all_embeddings_file and filters only those embeddings corresponding to the target_ids in tbf_file
    tbf_file: a .txt file with only product_ids that need to be filtered
    all_embeedings_file: a h5py file containing all the embeddings from which filtering needs to be done
    target_file_Path: a path name where the new h5py file with filtered embeddings need to be saved
    
    NOTE:
    NOTE:
    NOTE: This is a quick approach to acheive filtering but HIGHLY MEMORY INTENSIVE. So, for larger files and when less memory is available, use poor_filter_embeddings
    """
    print("Reading to be filtered prod_ids file")
    if(isinstance(to_be_filtered_file, pd.DataFrame)):
        print("Dataframe recognized")
        tbf_df = to_be_filtered_file
    elif(to_be_filtered_file.endswith('.txt')):
        print(".txt file recognized")
        tbf_df = pd.read_csv(to_be_filtered_file, names= ['p_id'])
#     print("Reading to be filtered prod_ids file Successful")
#     print("The reduced se df shape is: " + str(tbf_df.shape))
        r_se_pid = tbf_df['p_id'].values.tolist()
        p_id = [int(p) for p in r_se_pid]
        tbf_df = pd.DataFrame(r_se_pid, columns=["p_id"])
    elif(to_be_filtered_file.endswith('.h5py')):
        print(".h5py file recognized")
        p_name, e_name = get_dataset_names(to_be_filtered_file)
        fh = h5py.File(all_embeddings_file, 'r')
        p_id = fh[p_name][:]
        p_id = [int(p) for p in p_id]
        tbf_df = pd.DataFrame(p_id, columns=["p_id"])
    else:
        print("Couldn't determine the type of file! Please create a pandas dataframe with all the product_ids and pass it as p_id_df argument")
        print("Also, name the column 'p_id'")
    
    print("Reading the all embeddings file")
    f =  h5py.File(all_embeddings_file, 'r')
    prod_ids = f[pid][:]
    emb = f[embed][:]
    print("The all embeddings file read successfully")
    se_tup = list(zip(prod_ids,emb))
    concat_se_predmf_df = pd.DataFrame(se_tup, columns=['p_id', 'embed'])
    
    print("all embeddings file converted to df succesfully")
    
    print("Filtering in progress")
    filtered_df = tbf_df.merge(concat_se_predmf_df, on='p_id')
    print("The filtered df shape is:")
    print(filtered_df.shape)
    
    prod_ids = np.asarray(filtered_df['p_id'])
    embeds = np.asarray(filtered_df['embed'].values.tolist())
    
    print("Writing into target file h5py ")
    ff = h5py.File(target_file_path, 'w')
    pid_set = ff.create_dataset("p_id", (filtered_df.shape[0],), dtype='i8')
    emb_set = ff.create_dataset("emb", (filtered_df.shape[0],f[embed].shape[1]), dtype='f8')
    pid_set[:] = (prod_ids)
    emb_set[: , :] =(embeds)
    print("Writing to " + target_file_path + " successful!!!")
    ff.close()
    f.close()