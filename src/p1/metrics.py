import pandas as pd
import collections
import math
from statistics import mean


def compute_metrics(recs, labels, metric):
    """
    Computes information retrieval/ rankiing metrics
    :param recs: DataFrame with columns 'target', 'rec', 'score'
    :param labels: DataFrame with columns 'target', 'rec', 'rel' (stands for relevancy)
    :param metric: a function that takes 4 arguments:
        list of recommended Ids
        list of corresponding scores
        list of true correct ids
        list of true relevancies
    :return: float, average of the metric over all targets
    """
    jrecs = recs.groupby("target").agg(lambda x: list(x))
    jlabels = labels.groupby("target").agg(lambda x: list(x))

    joined = jrecs.join(jlabels, lsuffix = '_r', rsuffix = '_l', how='inner')

    metrics_by_target = joined.apply(lambda row: metric(row['rec_r'], row['score'], row['rec_l'], row['rel']), axis=1)
    return metrics_by_target.mean()


def NDCG(k=None):
    def DCG(rels):
        return sum([rel/math.log(pos + 2) for pos, rel in enumerate(rels)])

    def ndcg_calc(recs, scores, labels, rels):
        rels_dict = dict(zip(labels, rels))
        r = sorted(list(zip(recs, scores)), reverse=True, key=lambda x: x[1])
        rec_rels = [rels_dict.get(e[0], 0.0) for e in r]

        dcg = DCG(rec_rels) if k is None else DCG(rec_rels[:k])

        irels = sorted(rels, reverse=True)
        idcg = DCG(irels) if k is None else DCG(irels[:k])

        if idcg > 0:
            return dcg/idcg
        else:
            return 0.0

    return ndcg_calc
