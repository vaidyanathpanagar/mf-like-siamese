"""
Author: Vaidyanath Areyur Shanthakumar
Created: 06/2020
Description: This script takes input embeddings, saved_model and predicts output embeddings and saves them in a specified embedding file

all names need to be specified
"""

import math
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model
import statistics
import pandas as pd
import pickle
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from time import time
import h5py

se_embed_file = "/usr/app/Transformer/concat_se_plus_prod_attributes_3mil_count.h5py"
model_file_name = "/usr/app/Transformer/saved_model_simple_sepluspatt_to_mf_mse_loss_0_0011"
pred_embeddings_file = "/usr/app/Transformer/pred_mf_embeddings_from_saved_model_simple_sepluspatt_to_mf_mse_loss_0_0011.h5py"

def timer(func):
    '''
    A decorator function to time the function calls
    '''
    def new_func(*args, **kw):
        start_time = time()
        result = func(*args, **kw)
        end_time = time()
        print("Total time to run "
              + func.__name__ + " = "
              + str(end_time - start_time)
              + "s")
        return result
    return new_func

model = keras.models.load_model(model_file_name)
print("model loaded successfully!")

with h5py.File(se_embed_file, 'r') as f:
    steps = 10000
    last_batch_size = f['p_id'].shape[0]%steps
    i=0
    ff = h5py.File(pred_embeddings_file, 'w')
    pid_set = ff.create_dataset("p_id", (f['p_id'].shape[0],), maxshape=(int(15e6),), dtype='i8')
    emb_set = ff.create_dataset("emb", (f['p_id'].shape[0],100), maxshape=(int(15e6),100), dtype='f8')

    while(i< f['p_id'].shape[0]):
        prod_ids = f['p_id'][i:i+steps]
        emb = f['emb'][i:i+steps]
        input_data = np.asarray(emb)
        pred_data = model.predict(input_data)
        pid_set[i:i+steps] = [prod_ids]
        emb_set[i:i+steps,:] =(pred_data)
        print(i)
        i=i+steps
    print("Last batch computing....")
    prod_ids = f['p_id'][last_batch_size:]
    emb = f['emb'][last_batch_size:]
    input_data = np.asarray(emb)
    pred_data = model.predict(input_data)
    pid_set[last_batch_size:] = [prod_ids]
    emb_set[last_batch_size:,:] =(pred_data)
ff.close()
f.close()
print("Hurray! Everything Successful!")

