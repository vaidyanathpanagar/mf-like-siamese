import fasttext as ft
import numpy as np
import pandas as pd
import h5py
import time
import os
import statistics
import pickle

from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from numpy.linalg import norm
from keras.preprocessing.text import text_to_word_sequence


product_labels = "label_desc.txt"
product_desc = "product_desc.txt"
print("Beginning to read file")

desc_length_count = list()
with open(product_desc) as fp:
    p_desc = fp.read()

p_desc_list = p_desc.split('\n')

desc_split = []
for desc in p_desc_list:
    desc_split.append(text_to_word_sequence(desc))

i = 0
for desc in desc_split:
    desc_length_count.append(len(desc))
    i+=1
    print(i)
print("\nSuccessfully completed this script\n")
print("max_length is: " + str(max(desc_length_count)))
print(min(desc_length_count))
print(statistics.mean(desc_length_count))

pickle.dump(desc_length_count, open("prod_desc_lengths_list.p", 'wb'))
print("Pickle dumped successfully!")

print(max(desc_length_count))
print(min(desc_length_count))
print(statistics.mean(desc_length_count))

