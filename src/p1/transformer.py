import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import text_to_word_sequence
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import GlobalAveragePooling1D
from keras.layers import Dropout
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
# fix random seed for reproducibility
np.random.seed(7)
debug = False
import pickle
#from keras import layers
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.initializers import Constant
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

max_desc_length = 30
embedding_vector_length = 64
test_set_size = 0.5
batch_size = 50

class MultiHeadSelfAttention(layers.Layer):
    def __init__(self, embed_dim, num_heads=8):
        super(MultiHeadSelfAttention, self).__init__()
        self.embed_dim = embed_dim
        self.num_heads = num_heads
        if embed_dim % num_heads != 0:
            raise ValueError(
                f"embedding dimension = {embed_dim} should be divisible by number of heads = {num_heads}"
            )
        self.projection_dim = embed_dim // num_heads
        self.query_dense = layers.Dense(embed_dim)
        self.key_dense = layers.Dense(embed_dim)
        self.value_dense = layers.Dense(embed_dim)
        self.combine_heads = layers.Dense(embed_dim)

    def attention(self, query, key, value):
        score = tf.matmul(query, key, transpose_b=True)
        dim_key = tf.cast(tf.shape(key)[-1], tf.float32)
        scaled_score = score / tf.math.sqrt(dim_key)
        weights = tf.nn.softmax(scaled_score, axis=-1)
        output = tf.matmul(weights, value)
        #print(weights.shape)
        #print(output.shape)
        return output, weights

    def separate_heads(self, x, batch_size):
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.projection_dim))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, inputs):
        # x.shape = [batch_size, seq_len, embedding_dim]
        batch_size = tf.shape(inputs)[0]
        query = self.query_dense(inputs)  # (batch_size, seq_len, embed_dim)
        key = self.key_dense(inputs)  # (batch_size, seq_len, embed_dim)
        value = self.value_dense(inputs)  # (batch_size, seq_len, embed_dim)
        query = self.separate_heads(
            query, batch_size
        )  # (batch_size, num_heads, seq_len, projection_dim)
        key = self.separate_heads(
            key, batch_size
        )  # (batch_size, num_heads, seq_len, projection_dim)
        value = self.separate_heads(
            value, batch_size
        )  # (batch_size, num_heads, seq_len, projection_dim)
        attention, weights = self.attention(query, key, value)
        attention = tf.transpose(
            attention, perm=[0, 2, 1, 3]
        )  # (batch_size, seq_len, num_heads, projection_dim)
        concat_attention = tf.reshape(
            attention, (batch_size, -1, self.embed_dim)
        )  # (batch_size, seq_len, embed_dim)
        output = self.combine_heads(
            concat_attention
        )  # (batch_size, seq_len, embed_dim)
        print(output.shape)
        return output

class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super(TransformerBlock, self).__init__()
        self.att = MultiHeadSelfAttention(embed_dim, num_heads)
        self.ffn = keras.Sequential(
            [layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim),]
        )
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, inputs, training):
        attn_output = self.att(inputs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)

class TokenAndPositionEmbedding(layers.Layer):
    def __init__(self, maxlen, vocab_size, embed_dim):
        super(TokenAndPositionEmbedding, self).__init__()
        self.token_emb = layers.Embedding(input_dim=vocab_size, output_dim=embed_dim)
        self.pos_emb = layers.Embedding(input_dim=maxlen, output_dim=embed_dim)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions

class TokenAndPositionEmbeddingPretrained(layers.Layer):
    def __init__(self, maxlen, vocab_size, embed_dim, pre_trained_model_matrix):
        super(TokenAndPositionEmbeddingPretrained, self).__init__()
        self.token_emb = layers.Embedding(input_dim=vocab_size, output_dim=embed_dim, embeddings_initializer = pre_trained_model_matrix, trainable = False)
        self.pos_emb = layers.Embedding(input_dim=maxlen, output_dim=embed_dim, trainable= False)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions

def Tokenize_Desc(raw_desc):
    #reviews_en = reviews.split('\n')
    reviews_en = raw_desc
    all_text = ' '.join(reviews_en)
    words = all_text.split()
    
    if(debug):
        for word in words:
            if word == '\n':
                print(word)
        
    
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(words)
    print("Fitting is complete")
    
    rvs_split = []
    for r in reviews_en:
        rvs_split.append(text_to_word_sequence(r))
    print("rvs_split")
    print(rvs_split[:10])
    
    tok_rvs = []
    for r in rvs_split:
        tok_rvs.append(tokenizer.texts_to_sequences(r))
    print("Sequencing complete")
    print("tok_rvs")
    print(tok_rvs[:10])
    
    word_index = tokenizer.word_index
    print(len(word_index))
    
    if debug:
        print("The total number of different words is:" + str(len(word_index)))
    
    return tok_rvs

def Pad_Reviews(tok_rvs, max_review_length):
    pad = pad_sequences(tok_rvs, maxlen = max_review_length)
    print("train_pad is complete.")
    
    flat_pad = [[] for i in range(len(pad))]
    
    for count,x in enumerate(pad):
        for y in x:
            for z in y:
                flat_pad[count].append(z)
    
    enc_reviews = np.asarray(flat_pad)
    print("enc_reviews")
    print(enc_reviews[:10])
    return enc_reviews

def get_mf_filtered_labels(file_name, current_df):
    #data_path = "mf_filtered_se_embeddings_06_11_2020.p"
    data = pickle.load(open(file_name, "rb"))
    print("Data read successfully")
    print(data.shape)
    data.pop('embed_y')
    final_filt_ = current_df.merge(data, on='p_id')
    return final_filt_


mf_embed_path = "mf_filtered_se_embeddings_06_11_2020.p"

data = pickle.load(open(mf_embed_path, "rb"))
print("Data read successfully")
print(data.shape)

filtered_df = pickle.load(open("mf_filtered_raw_desc_df.p", "rb"))
prod_desc = list(filtered_df['description'])

tok_rvs = Tokenize_Desc(prod_desc)
padded_desc = Pad_Reviews(tok_rvs, max_desc_length)
final_filt = get_mf_filtered_labels("/home/vareyurshanthakumar/transformer/Transformer/mf_filtered_se_embeddings_06_11_2020.p", filtered_df)
labels = final_filt.pop('embed_x')
y_train = np.asarray(labels.values.tolist())
y_train = y_train.reshape([labels.shape[0],100])

def Eliminate_Empty_Reviews(enc_reviews,truth_stars):
    tr_s = truth_stars.tolist()
    enc_r = enc_reviews.tolist()
    
    #enc_r.pop(len(enc_r)-1)
    for i,r in enumerate(enc_r):
        if (is_allZeros(r)):
            enc_r.pop(i)
            tr_s.pop(i)
    
    enc_rev = np.asarray(enc_r)
    tr_rat = np.asarray(tr_s)
    
    return enc_rev, tr_rat

encoded_desc, final_labels = Eliminate_Empty_Reviews(padded_desc, y_train)
X_train, X_test, y_train, y_test = train_test_split(encoded_desc, final_labels, test_size = test_set_size, random_state=42)
vocab_size = 77915

num_heads = 2  # Number of attention heads
ff_dim = 64  # Hidden layer size in feed forward network inside transformer
Epochs = 25

inputs = layers.Input(shape=(max_desc_length,))
embedding_layer = TokenAndPositionEmbedding(max_desc_length, vocab_size + 1, embedding_vector_length)
x = embedding_layer(inputs)
transformer_block = TransformerBlock(embedding_vector_length, num_heads, ff_dim)
x = transformer_block(x)
x = transformer_block(x)
x = transformer_block(x)
x = layers.GlobalAveragePooling1D()(x)
#x = keras.layers.Dropout(0.1)(x)
#x = keras.layers.Dense(300, activation="relu")(x)
#x = keras.layers.Dropout(0.1)(x)
#outputs = keras.layers.Dense(100, activation="softmax")(x)
outputs = keras.layers.Dense(100)(x)

model = keras.Model(inputs=inputs, outputs=outputs)
model.summary()
print(X_train.shape)
print(X_test.shape)

rmsprop = tf.keras.optimizers.RMSprop(0.0001)
model.compile("adam", "mean_squared_error", metrics=["mean_squared_error"])
history = model.fit(X_train, y_train, batch_size = batch_size, epochs=Epochs, validation_data=(X_test, y_test))
#model.save goes here