import h5py
import pandas as pd
import tensorflow as tf


def read_emb_to_numpy(filepath):
    """
    Reads an h5 file with 2 datasets: 2-d array of embeddings and 1-d
    array of  ids (integers)
    :param filepath: h5 filepath
    :return: a tuple of 2-d ndarray and 1-d ndarray
    """
    with h5py.File(filepath, 'r') as f:
        if len(f.keys()) != 2:
            raise AssertionError("There should be exactly 2 datasets in the file: embeddings and product ids")
        emb_key, id_key = list(f.keys())
        if not 'emb' in emb_key:
            if 'emb' in id_key:
                emb_key, id_key = id_key, emb_key
            else:
                raise AssertionError("One of the datasets shoud be named 'emb' or 'embeddings' or similar")
        embs, ids = f[emb_key][:], f[id_key][:]
    print(f"Embeddings shape: {embs.shape}, id shape: {ids.shape}")
    return embs, ids


def write_emb_to_file(embs, ids, filepath, emb_ds_name='embeddings', ids_ds_name='product_ids'):
    """
    Write embeddings in a mflike format
    :param embs: 2-d ndarray of embedding vectors
    :param ids: 1-d array of corresponding product ids
    :param *name: dataset names inside the h5 file
    :param filepath: path to the file where the embeddings should be saved
    """
    with h5py.File(filepath, "w") as f:
        f[emb_ds_name] = embs.astype('f8')
        f[ids_ds_name] = ids.astype('i8')


def create_training_file(source_emb_file, label_emb_file, output_training_file):
    """
    From source and target (aka label) embeddings creates a file with 2 embeddings arrays
    and an array with corresponding product ids. All 3 in the same order. It only picks
    the common products from the 2 input files.
    :param source_emb_file:
    :param label_emb_file:
    :param output_training_file:
    :return: writes to the output_training_file
    """
    source_emb, source_ids = read_emb_to_numpy(source_emb_file)
    label_emb, label_ids = read_emb_to_numpy(label_emb_file)
    source_id_df = pd.DataFrame(source_ids, columns=['p_id']).reset_index().rename(columns={'index': 'se_ind'})
    label_id_df = pd.DataFrame(label_ids, columns=['p_id']).reset_index().rename(columns={'index': 'mf_ind'})
    common_ids_df = source_id_df.merge(label_id_df, on=['p_id'])
    with h5py.File(output_training_file, "w") as f:
        f['source_embedding'] = source_emb[common_ids_df['se_ind']]
        f['label_embedding'] = label_emb[common_ids_df['mf_ind']]
        f['product_ids'] = common_ids_df['p_id']


def split_ds(ds, splits, seed=None):
    """
    Splits a given dataset into train, validation and test datasets
    :param ds:
    :param splits: a length 3 list of relative fractions of each set
    e.g. [70, 15, 15]. Don't have to sum to 100.
    :param seed: random seed
    :return:  a triplet of 3 datasets (train, val, test)
    """
    total_size = tf.data.experimental.cardinality(ds).numpy()
    # ds = ds.shuffle(buffer_size=min(total_size, 2**16), seed=seed)
    split_fractions = [v / sum(splits) for v in splits]

    train_size = int(total_size * split_fractions[0])
    val_size = int(total_size * split_fractions[1])
    test_size = total_size - (train_size + val_size)

    train_ds = ds.take(train_size)
    val_ds = ds.skip(train_size).take(val_size)
    test_ds = ds.skip(train_size + val_size)
    return train_ds, val_ds, test_ds
