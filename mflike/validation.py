from collections import namedtuple
import faiss
import pandas as pd
import tensorflow as tf

from src.p1.metrics import compute_metrics

_ValidationData = namedtuple("ValidationData", ["se_emb", "ids", "targets_idx", "click_pairs"])


class ValidationData(_ValidationData):
    """A namedtuple representing data that is needed to evaluate mf-like model.
    se_emb:  2-d ndarray of input (SE) embeddings
    ids: item ids corresponding to embeddings
    targets_idx: indices (in ids array) of items to use as targets for eval
        all items are used as candidates
     click_pairs: a pd.DataFrame with eval click pairs. Has 3 columns:
        'target', 'rec', 'rel'
    """


def validate_mflike_model(model, validation_data, metrics):
    """
    Computes recommedations metrics given a tf model and input embeddings
    :param model: tf model mapping 1-d vectors to 1-d vectors
    :param validation_data: an instance of ValidationData
    :param metrics: a dictionary of metrics e.g.
    { "NDCG@5": NDCG(5),...}
    :return: a dictionary of metric names and values
    """
    k_nearest = 20
    mf_like_emb = model.predict(validation_data.se_emb)

    index = faiss.IndexFlatL2(mf_like_emb.shape[1])   # build the index
    index = faiss.IndexIDMap(index)
    index.add_with_ids(mf_like_emb, validation_data.ids)

    D, I = index.search(mf_like_emb[validation_data.targets_idx], k_nearest + 1)

    recs_df = pd.DataFrame(
        {'recs': pd.Series(I.tolist()),
         'dists': pd.Series(D.tolist())
         })

    recs_df['target'] = recs_df['recs'].apply(lambda l: l[0])
    recs_df['recs'] = recs_df['recs'].apply(lambda l: l[1:])
    recs_df['dists'] = recs_df['dists'].apply(lambda l: l[1:])

    recs_df = recs_df.explode(['recs','dists'])
    recs_df['score'] = 1 - recs_df['dists']
    recs_df = recs_df[['target', 'recs', 'score']].rename(columns={'recs': 'rec'})

    res = {name: compute_metrics(recs_df, validation_data.click_pairs, m)
           for name, m in metrics.items()}
    return res


class RecMetricCallback(tf.keras.callbacks.Callback):

    def __init__(self, validation_data, metrics, file_writer, is_siamese=False, freq=None):
        self.val_data = validation_data
        self.metrics = metrics
        self.writer = file_writer
        self.is_siamese = is_siamese
        self.freq = freq
        super(RecMetricCallback, self).__init__()

    def on_epoch_end(self, epoch, logs=None):
        super(RecMetricCallback, self).on_epoch_end(epoch, logs)
        if self.freq and not (epoch + 1) % self.freq == 0:
            return

        model = self.model.get_layer("branch") if self.is_siamese else self.model
        metrics = validate_mflike_model(model, self.val_data, self.metrics)
        print(metrics)
        with self.writer.as_default():
            for name, value in metrics.items():
                tf.summary.scalar(name, value, epoch)